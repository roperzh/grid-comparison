# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Nutrient.destroy_all
Product.destroy_all

30.times do
  print "."
  Nutrient.create(name: Faker::Name.first_name, weight: Faker::Number.decimal(2))
end

3000.times do
  print "."
  Product.create(name: Faker::Commerce.product_name, nutrients: Nutrient.all.sample(3))
end
