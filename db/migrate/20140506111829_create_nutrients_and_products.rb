class CreateNutrientsAndProducts < ActiveRecord::Migration
  def change
    create_table :nutrients do |t|
      t.string  :name
      t.integer :weight
      t.timestamps
    end

    create_table :products do |t|
      t.string :name
      t.timestamps
    end

    create_table :nutrients_products, id: false do |t|
      t.belongs_to :nutrient
      t.belongs_to :product
    end
  end
end
