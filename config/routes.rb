Rails.application.routes.draw do
  root to: redirect('/angular-table')

  get "/angular-table", to: "grids#angular_table"
  get "/ng-grid",       to: "grids#ng_grid"

  resources :products, only: [:index]
end
