// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require semantic-ui
//= require angular
//= require ng-route
//= require angular-table
//= require ng-grid
//= require_tree .

var app = angular.module('gridComparison', ['ngRoute', 'ngGrid', 'angular-table']);

app.factory('getData', function($http) {
  $('#spinner').addClass('loading');

  return function($scope) {
    $http({
      url: "/products?limit=1000",
      method: "GET",
    }).success(function(data, status, headers, config) {
      $scope.data = data;
      $('#spinner').removeClass('loading');
    }).error(function(data, status, headers, config) {
      alert("error loading data");
      $scope.status = status;
    });
  };
});

app.controller('ngGridCtrl', ['$scope', '$http', 'getData', function($scope, $http, getData) {
  $scope.gridOptions = {
    data: 'data',
    enablePinning: true
  };

  getData($scope);
}]);

app.controller('angTableCtrl', ['$scope', '$http', 'getData', function($scope, $http, getData) {
  getData($scope);
}]);
