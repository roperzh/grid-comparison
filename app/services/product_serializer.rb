class ProductSerializer

  attr_accessor :collection

  def initialize(collection)
    @collection = collection
  end

  def to_json(*args)
    collection.map do |product|
      {
        name: product.name,
        nutrients: get_string
      }
    end.to_json
  end

  def get_string
    string = ""
    5.times { string += " #{rand(36**rand(30)).to_s(36)} " }
    string
  end
end
