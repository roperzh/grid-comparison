class ProductsController < ApplicationController
  respond_to :json

  def index
    limit = params[:limit] || 1000

    render json: ProductSerializer.new(Product.all.limit(limit))
  end

end
